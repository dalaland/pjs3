import fs from "fs";
import produits from "./produits.js";

const PATH_TICKETS = 'F:\\fichiers\\bureau\\etudes\\dut2\\00 - PJS3\\30 - Workspace v2\\data\\extract\\tickets\\tickets.txt';
const PATH_DETAILS_TICKETS = 'F:\\fichiers\\bureau\\etudes\\dut2\\00 - PJS3\\30 - Workspace v2\\data\\extract\\details\\details_tickets.txt';


const codeCli = [
    'ALAUV', 'ALCLE', 'ALDUV', 'ANLER', 'ARBLA', 'BEAVI',
    'BEGAL', 'BETIZ', 'BRLEH', 'CECHA', 'CÉDAU', 'CEGRO',
    'CHBES', 'CHBEU', 'CHBON', 'CHDUD', 'CHGAS', 'CHGRA',
    'CHLAB', 'CHLEC', 'CHNIE', 'CHROU', 'CLHAL', 'CLJAN',
    'CLLEM', 'DACOU', 'DAMIC', 'DELE ', 'DEMAT', 'DIDAL',
    'DIGOM', 'ELDEL', 'ERBEC', 'ERDEB', 'ERDUR', 'ERSWE',
    'FAMAR', 'FRLAN', 'FRMIC', 'GAANT', 'GECAU', 'GESEF',
    'HALER', 'JAHEN', 'JALES', 'JÉBAZ', 'JEBOR', 'JEHER',
    'JEHOU', 'JELAZ', 'JELEC', 'JEMAN', 'JUBOY', 'LEHOS',
    'LODUR', 'LOHUE', 'MABON', 'MAEUS', 'MAFER', 'MAHEN',
    'MALAZ', 'MICOL', 'MIGAL', 'MOELK', 'NAROU', 'NISCH',
    'NOBEL', 'OLFER', 'OLLHE', 'PABEL', 'PAJAL', 'PALEC',
    'PALER', 'PAMAR', 'PASAU', 'PHFRO', 'PHSTI', 'PILED',
    'RARAB', 'REDAN', 'RIDAB', 'RIDAN', 'ROHAM', 'SAJOU',
    'SEADI', 'SELAU', 'SORET', 'SYBEL', 'SYLEJ', 'THLOP',
    'VIGUI', 'VILET', 'VIVAN', 'YAKAL', 'YATAN', 'ZOHED'
]

const NB_CAISSIER = 10;
const START_TICKET = 30000;
const MAX_ARTICLE = 30;
const MAX_QTE_ARTICLE = 3;
const REMISES = ["0,00", "0,05", "0,15", "0,25", "0,50"];

const POIDS_ND_TICKET = 300;
const MIN_TICKET_MOIS = POIDS_ND_TICKET / 5;

const ANNEE_DEPART = 2020;
const ANNEE_FIN = 2022;
const MOIS_DEPART = 0;
const MOIS_FIN = 12;

const NB_PROD = Object.keys(produits).length;
const PROD_KEYS = Object.keys(produits);




var TICKETS = '"NoTicket";"CodeCli";"Caissier";"DateTicket"\n';
var DETAILS_TICKETS = '"NoTicket";"Refprod";"PrixUnit";"Qte";"Remise%"\n';

var ticketStream = fs.createWriteStream(PATH_TICKETS, "utf-8");
ticketStream.write(TICKETS);
var detailTicketSream = fs.createWriteStream(PATH_DETAILS_TICKETS, "utf-8");
detailTicketSream.write(DETAILS_TICKETS);

console.time("Génération des tickets.");
var ticketEnCours = START_TICKET;
for(let i = ANNEE_DEPART; i <= ANNEE_FIN; ++i) {
    let annee = i;
    for (let j = MOIS_DEPART; j <= MOIS_FIN - 1; ++j) {
        let nbJours = daysInMonth(annee, j);
        let nbTicketMois = MIN_TICKET_MOIS + Math.round(POIDS_ND_TICKET * Math.random() * (1 - Math.random()));
        while (nbTicketMois > 0) {
            let jourDuMois = Math.ceil(Math.random() * (nbJours-1));
            //TICKETS += genTicket(ticketEnCours, annee, j, jourDuMois);
            // DETAILS_TICKETS += genDetailTicket(ticketEnCours);
            ticketStream.write(genTicket(ticketEnCours, annee, j, jourDuMois));
            detailTicketSream.write(genDetailTicket(ticketEnCours));
            nbTicketMois--; ticketEnCours++;
        }
    }
}

ticketStream.close();
detailTicketSream.close();
console.timeEnd("Génération des tickets.");


function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}
function ticketDate(annee, mois, jour) {
    let heure = Math.random() * (18 - 8) + 8;
    let date = new Date(annee, mois, jour, heure, Math.random() * 60, Math.random() * 60).toISOString();
    return date.replace(/-/g, '/').replace('T', ' ').slice(0, date.length - 5);
}
function genTicket(numero, annee, mois, jour) {
    let client = codeCli[Math.round(Math.random() * (codeCli.length - 1))];
    let caissier = Math.round(Math.random() * NB_CAISSIER) + 1;
    return numero + ';"' + client + '";' + caissier + ';' + ticketDate(annee, mois, jour) + '\n';
}
function getRemise() {
    if (Math.random() < 0.08) {
        return REMISES[Math.round(Math.random() * (REMISES.length-1))];
    }
    else
        return '0,00'
}



function genDetailTicket(numero) {
    let nb_article = Math.round(Math.random() * MAX_ARTICLE) + 1;
    let res = "";
    while (nb_article > 0) {
        nb_article--;
        let qte = Math.round(Math.random() * MAX_QTE_ARTICLE) + 1;

        let idx = Math.round(Math.random() * (NB_PROD-1));
        let key = PROD_KEYS[idx];

        res += numero + ';' + key + ";" + produits[key] + ";" + qte + ";" + getRemise() + '\n';
    }
    return res;
}
