#===================================================================#
#-------------------------------------------------------------------#
#                           Projet PJS3	                            #
#-------------------------------------------------------------------#
#*******************************************************************#
#   V0.1.1       Alexis Leon, Maxime Wang, Julien Comoli, Ita       #   
#                           19/11/21                                #
#                                                                   #
#===================================================================#

from matplotlib import pyplot as plt
import plotly.graph_objects as px 
import numpy as np
import pandas as pd

from ressources import ressources
from lot_1 import lot_1
from lot_2 import lot_2


# LOT 1
# Question 1 ------------------------------
def ca_mensuel(lot : lot_1) :
    lot.get_ca_mensuel().plot(x='Mois',y='CA')
    plt.title("Chiffre d\'affaire mensuel")
    plt.grid()
    plt.ylim(0)
    plt.show()

# Question 2 ------------------------------
def ticket_moyen(lot : lot_1) -> None : 
    lot.get_moyenne_ticket_mensuel().plot(x='Mois',y='Moyenne')
    plt.title("Moyenne mensuelle du montant des tickets")
    plt.grid()
    plt.ylim(0)
    plt.show()

# Question 3 ------------------------------
def ca_mensuel_produit(lot : lot_1, listeProduits) :
    if type(listeProduits) == list :
        comparaisonProduit(lot, listeProduits)
    elif type(listeProduits) == str :
        uniqueProduit(lot, listeProduits)
    else :
        print("Argument listeProduits incorrect")

def uniqueProduit(lot : lot_1, produit : str) -> None :
    lot.get_ca_produit(produit).plot(x='Mois')
    plt.title("Chiffre d'affaire pour les produits " + produit)
    plt.grid()
    plt.ylim(0)
    plt.show()

def comparaisonProduit(lot : lot_1, listeProduits : list[str]) -> None :
    ca_p = pd.DataFrame({'Mois' : lot.tickets['Mois'].unique()})
    first = True
    titre = "Comparaison du chiffre d'affaires des produits "

    for produit in listeProduits :
        if first :
            titre += produit
            first = False
        else :
            titre += ", " + produit

        ca_produit = lot.get_ca_produit(produit)

        ca_p = pd.merge(ca_p, ca_produit[['Mois', 'CA ' + produit]], on= 'Mois')
        
    titre = titre.rstrip(titre[-1:-4])

    ca_p.plot(x= 'Mois')
    
    plt.title(titre)
    plt.grid()
    plt.ylim(0)
    plt.show()

# LOT 2
# Question 4
def moyenne_ticket_client(lot : lot_2) -> None : 
    moyenneTicket = lot.get_moyenne_ticket_client()
    moyenneTicket.plot.bar(x='NomComplet', y='Montant moyen')
    plt.title("Montant ticket moyen par client")
    plt.grid()
    plt.ylim(0)
    plt.show()

# Question 5
def depense_mensuelle_produit_client(lot : lot_2, produit : str, client) -> None :
    if type(client) == str :
        uniqueClient(lot, produit, client)
    elif type(client) == list :
        comparaisonClient(lot, produit, client)
    else :
        print("Argument client incorrect")

def uniqueClient(lot : lot_2, produit : str, client : str) -> None :
    nom = lot.carteFidelites[lot.carteFidelites['CodeCli'] == client]['Nom'].head(1).item()
    prenom = lot.carteFidelites[lot.carteFidelites['CodeCli'] == client]['Prenom'].head(1).item()

    nomComplet = nom + " " + prenom

    lot.get_clients_par_produit(produit, client).plot()

    plt.title("Montant dépensé chaque mois par " + nomComplet)
    plt.grid()
    plt.ylim(0)
    plt.show()
    
def comparaisonClient(lot : lot_2, produit : str, client : list[str]) -> None :
    depenses = None
    titre = "Comparaison du montant dépensé pour le produit " + produit + " par "

    first = True
    for c in client :
        nom = lot.carteFidelites[lot.carteFidelites['CodeCli'] == c]['Nom'].head(1).item()
        prenom = lot.carteFidelites[lot.carteFidelites['CodeCli'] == c]['Prenom'].head(1).item()
        nomComplet = nom + " " + prenom
        if first :
            depenses = lot.get_clients_par_produit(produit, c)
            first = False
            titre += nomComplet
        else :
            d = lot.get_clients_par_produit(produit, c)
            depenses = pd.merge(depenses, d, on='Mois')
            titre += ", " + nomComplet

        depenses.rename(columns={'Depense Mensuelle' : nomComplet}, inplace = True)

    depenses.plot.bar()

    plt.title(titre)
    plt.grid()
    plt.ylim(0)
    plt.show()


# Extra
def moyenne_ticket_client_mensuelle(lot : lot_2) -> None : 
    moyenneTicket = lot.get_moyenne_ticket_client_mensuelle()
    moyenneTicket.plot.bar()
    plt.title("Montant ticket moyen par client")
    plt.grid()
    plt.ylim(0)
    plt.show()

def main() : 
    d = ressources()

    lot1 = lot_1(d)
    ca_mensuel(lot1)
    ticket_moyen(lot1)
    ca_mensuel_produit(lot1, 'vegan')
    ca_mensuel_produit(lot1, 'bio')
    ca_mensuel_produit(lot1, 'casher')
    ca_mensuel_produit(lot1, 'halal')
    ca_mensuel_produit(lot1, ["bio", "vegan", "halal", "casher"])

    lot2 = lot_2(d)
    moyenne_ticket_client(lot2)
    depense_mensuelle_produit_client(lot2, 'bio', ["ALAUV",'ALCLE'])
    print(lot2.get_liste_produits_client("bio", "ALCLE", "06/2021"))
    print(lot2.get_liste_produits_client("bio", "ALAUV"))
    depense_mensuelle_produit_client(lot2, 'bio', 'ALAUV')



if __name__ == '__main__':
  main()
