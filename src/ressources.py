#--------------------------------------------#
#          Importation des packages          #
#--------------------------------------------#
import pathlib
import os
import sys
import glob
import pandas as pd

#--------------------------------------------#
#                    Code                    #
#--------------------------------------------#

class ressources :

	produits = pd.DataFrame()
	categories = pd.DataFrame()
	carteFidelites = pd.DataFrame()
	tickets = pd.DataFrame()
	detailsTickets = pd.DataFrame()

	def __init__(self) -> None:
		pd.options.display.float_format = '{:.2f}'.format
		self._init_constants()
		self._init_donnees_mensuelles()
		self._formatage_donnees()
	
	def _init_constants (self) :
		#initialisation fichiers constants
		pathProduit = os.path.join(pathlib.Path(__file__).parent.absolute(), "../ressources/constants/Produit.txt")
		pathCategorie = os.path.join(pathlib.Path(__file__).parent.absolute(), "../ressources/constants/Categorie.txt")
		pathCarteFidelite = os.path.join(pathlib.Path(__file__).parent.absolute(), "../ressources/constants/CarteFidelite.txt")

		msgErreurFichier = "impossible d'ouvrir le fichier : "

		if os.path.exists(pathProduit) :
			self.produits = pd.read_csv(pathProduit, sep=';')
		else :
			print(msgErreurFichier + pathProduit)
			sys.exit()

		if os.path.exists(pathCategorie) :
			self.categories = pd.read_csv(pathCategorie, sep=';')
		else :
			print(msgErreurFichier + pathCategorie)
			sys.exit()

		if os.path.exists(pathCarteFidelite) :
			self.carteFidelites = pd.read_csv(pathCarteFidelite, sep=';')
		else :
			print(msgErreurFichier + pathCarteFidelite)
			sys.exit()

	def _init_donnees_mensuelles (self) :
		for directory in glob.iglob(os.path.join(pathlib.Path(__file__).parent.absolute(), "../ressources/tickets-mensuel/*")) : #par ordre alphabetique
			for file in glob.iglob(directory + "/Ticket*") :
				self.tickets = pd.concat([self.tickets, pd.read_csv(file, sep=';')])
			for file in glob.iglob(directory + "/DetailTicket*") :
				self.detailsTickets = pd.concat([self.detailsTickets, pd.read_csv(file, sep=';')])

	def _formatage_donnees (self) :
		self.produits['PrixUnit'].replace('€','', inplace = True, regex=True) #Suppression du signe €
		self.produits['PrixUnit'].replace(',','.', inplace = True, regex=True) #Transformation , en .
		self.produits['PrixUnit'] = pd.to_numeric(self.produits['PrixUnit']) #Transformation en float

		self.carteFidelites['DateNaiss'] = pd.to_datetime(self.carteFidelites['DateNaiss'], dayfirst=True) #Convertir le format de la date en yyyy-mm-dd
		self.carteFidelites['Jour'] = self.carteFidelites['DateNaiss'].dt.day #Creation d'une colonne contenant le num de jour
		self.carteFidelites['Mois'] = self.carteFidelites['DateNaiss'].dt.month #Creation d'une colonne contenant le num de mois
		self.carteFidelites['Année'] = self.carteFidelites['DateNaiss'].dt.year #Creation d'une colonne contenant l'annee
		self.carteFidelites['Nom'].replace(' ','', inplace = True, regex=True) #Suppression espaces multiples
		self.carteFidelites['Nom'].replace('_','', inplace = True, regex=True) #Suppression underscore
		self.carteFidelites['Prenom'].replace(' ','', inplace = True, regex=True) #Suppression espaces multiples
		self.carteFidelites['Prenom'].replace('_','', inplace = True, regex=True) #Suppression underscore

		self.tickets['DateTicket'] = pd.to_datetime(self.tickets['DateTicket'], dayfirst=True) #Convertir le format de la date en yyyy-mm-dd
		self.tickets['Mois'] = self.tickets['DateTicket'].dt.month.astype(str).map(str) + "/" + self.tickets['DateTicket'].dt.year.astype(str) 
		self.tickets['Mois'] = self.tickets.apply(lambda x:self.date_format(x['Mois']), axis= 1)

		self.detailsTickets['PrixUnit'].replace('€','', inplace = True, regex=True) #Suppression du signe €
		self.detailsTickets['PrixUnit'].replace(',','.', inplace = True, regex=True) #Transformation , en .
		self.detailsTickets['PrixUnit'] = pd.to_numeric(self.detailsTickets['PrixUnit']) #Transformation en float

		self.detailsTickets['Remise%'].replace(',','.', inplace = True, regex=True) #Transformation , en .
		self.detailsTickets['Remise%'] = pd.to_numeric(self.detailsTickets['Remise%']) #Transformation en float

	def date_format(self, date) :
		if len(date) < len("mm/aaaa") :
			return '0' + date
		else :
			return date
