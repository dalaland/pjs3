import pandas as pd

from ressources import ressources

class lot_1 :

    def __init__(self, d : ressources) -> None:
        self.tickets = d.tickets
        self.detailsTickets = d.detailsTickets
        self.produits = d.produits


    def get_ca_mensuel(self) -> pd.DataFrame:
        infosTickets = pd.merge(self.tickets[["NoTicket","Mois"]], self.detailsTickets[["NoTicket","PrixUnit","Qte","Remise%"]], on = "NoTicket")

        # Calcul du coût 
        infosTickets['CoutVente'] = -(infosTickets['PrixUnit']*infosTickets['Qte']) * (infosTickets['Remise%'] - 1)
        
        # Calcul de la somme par mois 
        CAmensuel = infosTickets.groupby(['Mois'], as_index = False)['CoutVente'].sum()
        CAmensuel.rename(columns={'CoutVente': 'CA'}, inplace=True)

        return CAmensuel


    def get_moyenne_ticket_mensuel(self) -> pd.DataFrame:
        infosTickets = pd.merge(self.tickets[["NoTicket","Mois"]], self.detailsTickets[["NoTicket","PrixUnit","Qte","Remise%"]], on = "NoTicket")
        infosTickets['CoutVente'] = infosTickets['PrixUnit']*infosTickets['Qte'] * (1 - infosTickets['Remise%'])

        # Calcul du prix payé pour chaque produit
        coutTicket = infosTickets.groupby(['Mois','NoTicket'], as_index = False)['CoutVente'].sum()

        # Calcul de la moyenne
        moyenneTicketMensuel = coutTicket.groupby(['Mois'], as_index = False)['CoutVente'].mean()
        moyenneTicketMensuel.rename(columns={'CoutVente':'Moyenne'}, inplace = True)

        return moyenneTicketMensuel


    def get_ca_produit(self, produit : str) -> pd.DataFrame:
        infosTickets = pd.merge(self.tickets[["NoTicket","Mois"]], self.detailsTickets[["NoTicket","PrixUnit","Qte","Remise%","Refprod"]], on = "NoTicket")
        infosTickets = pd.merge(infosTickets[["NoTicket","Mois","Refprod","PrixUnit","Qte","Remise%"]], self.produits[["Nomprod", "Refprod"]], on = "Refprod")

        # Selection du produit
        produitSelectionne = infosTickets[(infosTickets['Nomprod'].str.lower().str.startswith(produit) | infosTickets['Nomprod'].str.contains('(?i) ' + produit)) & (infosTickets['Nomprod'].str.contains('(?i)' + produit + ' ') | infosTickets['Nomprod'].str.lower().str.endswith(produit))]

        # Calcul du coût pour chaque produit
        produitSelectionne.insert(7, 'CA ' + produit, -(produitSelectionne['PrixUnit']*produitSelectionne['Qte']) * (produitSelectionne['Remise%'] - 1))
        
        # Somme de tous les produits de la catégorie
        produitMensuel = produitSelectionne.groupby(["Mois"], as_index = False)['CA ' + produit].sum()

        return produitMensuel