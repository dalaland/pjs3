from matplotlib import pyplot as plt
import pandas as pd

from ressources import ressources

class lot_2 :

      def __init__(self, d : ressources) -> None:
          self.tickets = d.tickets
          self.detailsTickets = d.detailsTickets
          self.carteFidelites = d.carteFidelites
          self.produits = d.produits

      # Demande lot 2
      
      def get_moyenne_ticket_client(self) -> pd.DataFrame:
            # Crée une table à partir des tickets et de la liste des clients
            infosTickets = pd.merge(self.tickets[["NoTicket","CodeCli"]], self.carteFidelites[["CodeCli","Nom","Prenom"]], on = "CodeCli")

            # Ajout à la table du détail des tickets
            infosTickets = pd.merge(infosTickets[["NoTicket", "CodeCli","Nom","Prenom"]], self.detailsTickets[["NoTicket","PrixUnit","Qte","Remise%"]], on = "NoTicket")

            # Cout de vente d'un produit d'un ticket
            infosTickets.insert(4, 'NomComplet', infosTickets['Nom'].map(str) + " " + infosTickets['Prenom'])
            infosTickets.insert(8, 'Prix', infosTickets['PrixUnit']*infosTickets['Qte'] * (1 - infosTickets['Remise%']))

            # Somme du détail de chaque ticket
            ticketClient = infosTickets.groupby(["CodeCli","NomComplet", "NoTicket"], as_index=False)['Prix'].sum()

            # Moyenne des tickets pour chaque client
            moyenneClient = ticketClient.groupby(["CodeCli","NomComplet"], as_index=False)['Prix'].mean()
            moyenneClient.rename(columns={'Prix':'Montant moyen'}, inplace = True)

            moyenneClient = moyenneClient.sort_values(by = ['Montant moyen'], ascending = False)
            return moyenneClient

      def get_clients_par_produit(self, produit : str, client : str) -> pd.DataFrame:
            # Regroupe les infos nécessaires
            infosTickets = pd.merge(self.tickets[["NoTicket","Mois", "CodeCli"]], self.detailsTickets[["NoTicket","PrixUnit","Qte","Remise%","Refprod"]], on = "NoTicket")
            infosTickets = pd.merge(infosTickets[["NoTicket","Mois","PrixUnit", "CodeCli", "Qte","Remise%", "Refprod"]], self.produits[["Nomprod", "Refprod"]], on = "Refprod")
            
            # Retirer tous les produits en dehors de la catégories
            produitSelectionne = infosTickets[(infosTickets['Nomprod'].str.lower().str.startswith(produit) | infosTickets['Nomprod'].str.contains('(?i) ' + produit)) & (infosTickets['Nomprod'].str.contains('(?i)' + produit + ' ') | infosTickets['Nomprod'].str.lower().str.endswith(produit))]
            # Retirer tous les tickets ne correspondants pas au client
            clientSelectionne = produitSelectionne[produitSelectionne['CodeCli'].str.contains('(?i)' + client)]

            # Cout dépenser par produit
            clientSelectionne.insert(4, 'Prix', clientSelectionne['PrixUnit']*clientSelectionne['Qte'] * (1 - clientSelectionne['Remise%']))

            # Ajout de la liste de clients
            clientSelectionne = pd.merge(clientSelectionne[["Mois", "Prix","CodeCli"]], self.carteFidelites[["CodeCli","Nom","Prenom"]], on = "CodeCli")
            clientSelectionne['NomComplet'] = clientSelectionne['Nom'].map(str) + " " + clientSelectionne['Prenom']

            clientSelectionne.drop(columns=["Nom", "Prenom"],inplace=True)

            # Somme pour un produit
            sommeMensuelle = clientSelectionne.groupby(['Mois', 'NomComplet'], as_index=False)['Prix'].sum()
            sommeMensuelle.rename(columns={'Prix':'Depense Mensuelle'}, inplace = True)

            sommeMensuelle.set_index(['Mois', 'NomComplet'], inplace=True)

            return sommeMensuelle


      # Extra
      # Meme que 1ere demande mais moyenne par mois au lieu de total
      def get_moyenne_ticket_client_mensuelle(self) -> pd.DataFrame:
            # Crée une table à partir des tickets et de la liste des clients
            infosTickets = pd.merge(self.tickets[["NoTicket",'Mois',"CodeCli"]], self.carteFidelites[["CodeCli","Nom","Prenom"]], on = "CodeCli")

            # Ajout à la table du détail des tickets
            infosTickets = pd.merge(infosTickets[["NoTicket",'Mois', "CodeCli","Nom","Prenom"]], self.detailsTickets[["NoTicket","PrixUnit","Qte","Remise%"]], on = "NoTicket")

            # Cout de vente d'un produit d'un ticket
            infosTickets.insert(4, 'NomComplet', infosTickets['Nom'].map(str) + " " + infosTickets['Prenom'])
            infosTickets.insert(8, 'Prix', infosTickets['PrixUnit']*infosTickets['Qte'] * (1 - infosTickets['Remise%']))

            # Somme du détail de chaque ticket
            ticketClient = infosTickets.groupby(["CodeCli","NomComplet", 'Mois', "NoTicket"], as_index=False)['Prix'].sum()

            # Moyenne des tickets pour chaque client
            moyenneClient = ticketClient.groupby(["CodeCli","NomComplet", 'Mois'], as_index=False)['Prix'].mean()
            moyenneClient.rename(columns={'Prix':'Montant moyen'}, inplace = True)
            
            moyenneClient.set_index(["CodeCli","NomComplet", 'Mois'], inplace=True)

            return moyenneClient

      # Consulter la liste des produits d'une catégorie achetés par un client par mois
      def get_liste_produits_client(self, produit : str, client : str, mois = None) -> pd.DataFrame :
            # Regroupe les infos nécessaires
            infosTickets = pd.merge(self.tickets[["NoTicket","Mois", "CodeCli"]], self.detailsTickets[["NoTicket","PrixUnit","Qte","Remise%","Refprod"]], on = "NoTicket")
            infosTickets = pd.merge(infosTickets[["NoTicket","Mois","PrixUnit", "CodeCli", "Qte","Remise%", "Refprod"]], self.produits[["Nomprod", "Refprod"]], on = "Refprod")
            
            # Retirer tous les produits en dehors de la catégories
            produitSelectionne = infosTickets[(infosTickets['Nomprod'].str.lower().str.startswith(produit) | infosTickets['Nomprod'].str.contains('(?i) ' + produit)) & (infosTickets['Nomprod'].str.contains('(?i)' + produit + ' ') | infosTickets['Nomprod'].str.lower().str.endswith(produit))]
            clientSelectionne = produitSelectionne[produitSelectionne['CodeCli'].str.contains('(?i)' + client)]

            if mois != None :
                  moisSelectionne = clientSelectionne[clientSelectionne['Mois'].str.contains('(?i)' + mois)]
                  listeProduits = moisSelectionne
            else :
                  listeProduits = clientSelectionne

            # Cout dépenser par produit
            listeProduits.insert(4, 'Prix', listeProduits['PrixUnit']*listeProduits['Qte'] * (1 - listeProduits['Remise%']))

            # Ajout de la liste de clients
            listeProduits = pd.merge(listeProduits[["NoTicket","Mois", "Nomprod", "Prix","CodeCli"]], self.carteFidelites[["CodeCli","Nom","Prenom"]], on = "CodeCli")
            listeProduits['NomComplet'] = listeProduits['Nom'].map(str) + " " + listeProduits['Prenom']

            listeProduits.drop(columns=["Nom", "Prenom"],inplace=True)

            # Liste des dépense pour un produit
            listeProduits.set_index(["CodeCli","NomComplet", 'Mois'], inplace=True)
            listeProduits.sort_values(by = ['Mois'], inplace = True)


            return listeProduits





